/// 2023 by Alex 'Taulex' Mai aka CptMaister


/// some enums

const VARIATION_ENUM = {
  linear: "linear",
  polygon: "polygon",
  circle: "circle",
  radial: "radial",
  dvd: "dvd",
}
const VARIATION_ENUMS = Object.values(VARIATION_ENUM);

const SPHERE_SHAPE_ENUM = {
  circle: 0,
  square: 1,
  triangle: 2,
  diamond: 3,
  heart: 4,
  indexNumber: 5,
  indexNumberReverse: 6,
  numberOfImpacts: 7,
}


/// settings

const startAnimationAtMilli = 0;
let startTimeMilli = getTimeNow(); // time at which the animation starts, so now
let zeroTimeMilli; // time at which the animation is at its reset point

function resetZeroTime() {
  zeroTimeMilli = startTimeMilli - startAnimationAtMilli + 1; // time at which the animation is at its reset point // the "plus 1" fixes minor timing bugs
}
resetZeroTime();

const currentAmountOfSoundBites = 21; // this is not really a variable, but the amount of sound bites currently saved (per instrument) // TODO remove this completely

const singleWayTimeMilli = 1000; // this is exactly 1000 because we decided that moveLinePositionAtTime functions need time 0 to 1000 to get from one pulse line to the next
const singleWayTimeSeconds = 1;

const instruments = {

  // gönnen: https://musiclab.chromeexperiments.com/Experiments <- benutzen anscheinend Tone.js
  // oscillator sounds: https://musiclab.chromeexperiments.com/Oscillators/


  vibraphone: { // files pulled from Hyperplexed's video's codepen
    type: "files",
    amount: 21,
    pathStart: "vibraphone/vibraphone-key-",
    pathEnd: ".mp3",
    volume: 0.1
  },
  orchestralHarp: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "orchestral-harp.json",
    volume: 0.9
  },
  kalimba: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "kalimba.json",
    volume: 0.7
  },
  timpani: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "timpani.json",
    volume: 0.6
  },
  tubularBells: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "tubular-bells.json",
    volume: 0.6
  },
  acousticGrandPiano: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "acoustic-grand-piano.json",
    volume: 0.6
  },
  acousticGuitarNylon: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "acoustic-guitar-nylon.json",
    volume: 0.6
  },
  xylophone: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "xylophone.json",
    volume: 0.6
  },
  violin: { // from https://tympanus.net/Development/MusicalInteractions/#
    type: "base64",
    path: "violin.json",
    volume: 0.6
  }

  // TODO check https://github.com/surikov/webaudiofont
}

const mainSettings = {
  instrument: instruments.xylophone, // choose from instruments object above

  moveLinesAmount: 30, // this should not be higher than currentAmountOfSoundBites

  reverseMoveLinesOrder: false,
}

const soundSettings = {
  audioVolume: 1, // will be multiplied with by each instrument's volume

  reverseSoundsOrder: false,

  directionalSound: false, // TODO implement
}

const durationSettings = {
  pulsesAmountByFirstSphere: 100, // amount of pulse lines (i.e. sounds) the first sphere will pass after fullAnimationTimeSeconds many seconds. 100 is a good one

  pulsesDifferenceStyle: "sub", // "sub" | "add" | "mult" | "div" // set how the amount of cleared pulses will be different for each following circle
  pulsesDifferenceNumber: 2, // difference of amount of loops between circles

  fullAnimationTimeSeconds: 225, // 900 seconds is 15 minutes // this much time for the first sphere to clear pulsesAmountByFirstSphere many pulses
  fullAnimationsAmount: 0, // after this many full animation loops, the whole thing stops. Use 0 for unlimited. TODO use
  generalAnimationSpeedMultiplier: 1, // just make things faster or slower, with 1 being 100% speed
}

const variationSettings = { // TODO implement everything here
  variation: VARIATION_ENUM.radial, // any of VARIATION_ENUM: linear | polygon | circle | radial | dvd

  variationLinearPulseLinesAmount: 1,
  variationLinearPulseAtLimit: true, // at false the pulse lines are not placed at the limits, at true the first one is at the limits, with the two limit pulse lines acting as one pulse line
  variationLinearMoveLinesAtLimit: false, // at false the pulse lines are not placed at the limits, at true the first and last one is at the limits
  variationLinearDirection: "right", // "right" | "top" | "left" | "bottom"
  variationLinearBounce: true, // go back from the end, instead of teleporting to the start. If variationLinearPulseAtLimit is true, then the limit Pulse will be played on either end
  variationLinearBoundingBox: true,
  variationLinearWidthScale: 1,

  variationPolygonEdgesAmount: 4, // at least 3 is nice, 1 and 2 also work
  variationPolygonPulseLineDivisor: 1, // should be a divisor of variationPolygonEdgesAmount, handle in FrontEnd, maybe allow other integers as well
  variationPolygonPulseLinesShift: 0,
  variationPolygonReverseDirection: false, // for false go counter-clockwise, for true clockwise
  variationPolygonMoveLinesAtLimit: false, // at false the pulse lines are not placed at the limits, at true the first and last one is at the limits
  variationPolygonCenterEmpty: 0.2, // inner empty space, between 0 and 1, where 1 is the whole size
  variationPolygonCenterPulseLinesExtend: false, // makes the pulse lines extend into the center
  variationPolygonBounce: false,
  variationPolygonDelivery: false, // emulates the trick from the video https://www.youtube.com/watch?v=v46MnsZjPNo // recommended to use pulseLinesVisible false with this one
  variationPolygonDeliveryInverted: false, // only works if variationPolygonDelivery is true, inverts the delivery effect
  variationPolygonDeliveryIgnoreDivisors: true, // ignore variationPolygonPulseLineDivisor
  variationPolygonBoundingOuter: false,
  variationPolygonBoundingInner: true, // ignored if variationPolygonCenterEmpty is 0
  variationPolygonRotation: 0, // in degrees. At 0, the spheres start at the top, going in counter-clockwise rotational direction

  variationCirclePulseLinesAmount: 2,
  variationCircleArcDegrees: 360, // in degrees, between 1 and 360
  variationCircleMoveLinesAtLimit: false, // at false the pulse lines are not placed at the limits, at true the first and last one is at the limits
  variationCircleStartAtOtherEnd: false, // at false the spheres start at the right end of the arcs, at true at the left
  variationCircleCenterEmpty: 0.2, // inner circle empty space, between 0 and 1, where 1 is the size of the outer circle
  variationCircleCenterPulseLinesExtend: false, // makes the pulse lines extend into the center
  variationCircleBounce: false, // if false, circles will just teleport to their start, which only looks natural if variationCircleArcDegrees is a multiple of 360, else true is recommended here
  variationCircleBounceOffset: false, // if true, the first pulse line (which bounds the arcs) will be move down according to the sphereWidth and the degrees remaining for (360 - variationCircleArcDegrees) such that the spheres acutally bounce with their edge and not their center // TODO implement properly with angle adjustment
  variationCircleBoundingOuter: false,
  variationCircleBoundingInner: true, // ignored if variationCircleCenterEmpty is 0
  variationCircleRotation: 90, // in degrees. At 0, the center of the arc is at the top. The spheres start at the right end of the arcs.

  // TODO concept radial
  // variationRadialPulseLinesAmount: 1,
  variationRadialArcDegrees: 360, // in degrees, emitting area of spheres, any number from 1 is good, values above 360 are different from their (mod 360)-equivalent
  variationRadialDistributionFromCenter: true,
  variationRadialStartAtInside: true, // at false the spheres start at the outside, at true at the inside
  variationRadialCenterRadius: 0.2, // inner circle size, between 0 and 1, where 1 is the size of the outer circle
  variationRadialBounceAtInner: false, // will always be true if variationRadialCenterRadius is higher than 0
  variationRadialAdjustBounceBySphereWidth: true,
  variationRadialBoundingOuter: true,
  variationRadialBoundingInner: true, // ignored if variationRadialCenterRadius is 0
  variationRadialRotation: 0, // in degrees. At 0, the center of the arc is at the top

  // let's only do integers here, only scale is ok as non-integer
  variationDvdWidth: 16,
  variationDvdHeight: 9,
  variationDvdSphereSlopeX: 1,
  variationDvdSphereSlopeY: 1,
  variationDvdBounce: true, // else the box is more like an planar torus representation
  // variationDvdSpheresAmount: 1,
  // variationDvdTrace: false,
  // variationDvdSoundsPerSphere: 1,
  // variationDvdImpactColorChange: false,
  variationDvdBoundingBox: true,
  variationDvdBounceBoundingSizeAdjustment: true,
  variationDvdHeightScale: 1, // scale height without changing the logic
  variationDvdWidthScale: 1, // scale width without changing the logic
}

const styleSettings = {
  animationSizeScale: 1, // at 1 the size is (0.8 * minDim), with minDim equal to Math.min(viewportWidth, viewportHeight)
  animationShiftPosition: {x: 0, y: 0}, // shifts the animationCenter by this vector (in multiples of the animationSize)

  backgroundColor: "black",
  textColor: "white",

  sphereShape: SPHERE_SHAPE_ENUM.circle, // string | string[] | any from SPHERE_SHAPE_ENUM: circle | square | triangle | diamond | heart | indexNumber | indexNumberReverse | numberOfImpacts
  sphereShapeNumberAdd: 0, // gets added to the sphereShape number, if it is displaying a number
  sphereShapeNumberAddPerIndex: 0, // gets added to the sphereShape number, if it is displaying a number, depending on the index number of the sphere
  sphereShapeNumberMult: 1, // gets multiplied to the sphereShape number, if it is displaying a number, after the addition of sphereShapeNumberAdd
  sphereShapeNumberExpOf: 0, // if sphereShape is a number, after the addition of sphereShapeNumberAdd and such, this number will used as the base, the sphere number as exponent, result will be shown // special value 0 ignores this value // careful not to use too large numbers here
  sphereColorMode: "single", // "single" | "index" | "indexReverse" | "collisions" // with "single" for every sphere sphereColor is used. With "index", "indexRevers" and "collisions" the colors of sphereColorArray are used in some way (TODO concept) depending on the corresponding number.
  sphereColor: "white",
  sphereWidth: 0.03,
  sphereOutlineColor: "white",
  sphereOutlineWidth: 0.004,
  spherePulseColor: false, // TODO implement

  pulseLinesAboveMoveLines: true,

  pulseLinesVisible: true,
  pulseLineColor: "white",
  pulseLineWidth: 0.011,

  moveLinesVisible: true,
  moveLineColorHSLHue: "colorGradientReverse", // "colorGradient" | "colorGradientReverse" | any hue value of the color wheel, from 0 to 360, see   // e.g. "gray" or "#00ff00"
  moveLineColorHSLSaturation: 1,
  moveLineWidth: 0.007,

  fadeOutImpactPulseTimeMilli: 400,
  moveLineSilenceOpacity: 1,
  moveLineSilenceLightness: 0.1,
  moveLinePulseOpacity: 1.0,
  moveLinePulseLightness: 0.6,
  moveLinePulseWidthExtra: 0.6, // can be negative

  connectionLineSpheres: "center", // "neighbors" | "neighborsMod" | "center" | "spheres" | "centerAndNeighbors" | "centerAndNeighborsMod" | number | number[] (only implemented for "linear") | anything else for no connection lines // draw a straight line connecting each sphere with its up to two index neighbor spheres, with the center of the animation, or with all other spheres
  connectionLineColor: "white",
  connectionLineWidth: 0.003,

  boundingColor: "aquamarine",
  boundingInnerFill: "black", // fills the inner space if there is an inner bounding
  boundingWidth: 0.011,
}


/// one time only setup stuff

// some repeatedly used numbers for some settings
const sqrtTwoAppr = 1.4142;
const sqrtThreeAppr = 1.7321;
const sqrtThreeApprDivByFour = 0.4330;
const sqrtThreeApprDivBySix = 0.2887;

// html elements
const body = document.getElementById("body");
body.onkeydown = handleKeyDown;
const paper = document.getElementById("paper");
const pen = paper.getContext("2d"); // see https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D

const divUi = document.getElementById("divUi");
const overlayCenter = document.getElementById("overlayCenter");

const spanTimer = document.getElementById("spanTimer");

const emojiPlay = "▶️";
const emojiPause = "⏸️";

const buttonSoundToggle = document.getElementById("buttonSoundToggle");
const buttonAnimationToggle = document.getElementById("buttonAnimationToggle");
const buttonSettingsMenu = document.getElementById("buttonSettingsMenu");
const spanInfoToggleSound = document.getElementById("spanInfoToggleSound");
const buttonRestartAnimation = document.getElementById("buttonRestartAnimation");
const buttonVisibilityToggle = document.getElementById("buttonVisibilityToggle");
const buttonTimerSinceStart = document.getElementById("buttonTimerSinceStart");
const buttonChangeVariation = document.getElementById("buttonChangeVariation");


// styling stuff
body.style.backgroundColor = styleSettings.backgroundColor;
body.style.color = styleSettings.textColor;


// audio
let audioContext;
let gainNode;

function initializeAudioContext() {
  try {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();

    gainNode = audioContext.createGain();
    gainNode.gain.value = mainSettings.instrument.volume * soundSettings.audioVolume; // audio volume
    gainNode.connect(audioContext.destination);

    try {
      loadAudioBuffers();
    } catch(e) {
      console.warn("Loading audio files failed.")
    }
  } catch(e) {
    console.warn('Web Audio API is not supported in this browser');
  }
}

async function loadAudioBuffers() {

  const instrument = mainSettings.instrument;

  // build promises to get audioBuffers
  let audioBufferPromises;

  if (instrument.type === "files") {
    audioBufferPromises = paths.map((path, index) => { // just loop over the paths to load the proper amount
      return fetchAudioBufferPromiseFromFile(getAudioPath(index));
    })

  } else if (instrument.type === "base64") {
    const instrumentSoundsPromise = fetch("./audio/" + instrument.path)
      .then((response) => response.json());
    const instrumentSounds = await instrumentSoundsPromise;

    audioBufferPromises = paths.map((path, index) => { // just loop over the paths to load the proper amount
      return fetchAudioBufferPromiseFromBase64(instrumentSounds[
        soundSettings.reverseSoundsOrder ?
        instrumentSounds.length - (index % instrumentSounds.length) - 1 :
        (index % instrumentSounds.length)
      ]);
    })

  } else {
    console.warn("Instrument setup is messed up, unrecognized type.", instrument)
  }

  // wait for audio buffer promises and then add audioBuffers to paths
  Promise.all(audioBufferPromises).then(buffers => {
    const newPaths = paths.map((path, index) => ({
      ...path,
      audioBuffer: buffers[index]
    }));

    paths.splice(0, paths.length, ...newPaths)
  })
}

function fetchAudioBufferPromiseFromFile(path) {
  return fetch(path) // return this Promise
    .then((res) => res.arrayBuffer())
    .then((arrayBuffer) => audioContext.decodeAudioData(arrayBuffer));
}


function dataURLtoFile(dataurl, filename) {
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[arr.length - 1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, {type:mime});
}

function fetchAudioBufferPromiseFromBase64(base64) {
  const oggFile = dataURLtoFile(base64,`tmp-file-name.ogg`);
  return oggFile.arrayBuffer()
    .then((arrayBuffer) => audioContext.decodeAudioData(arrayBuffer));
}

function getAudioPath(index) {
  const modIndex = index % mainSettings.instrument.amount;
  return `./audio/${
    mainSettings.instrument.pathStart
  }${
    soundSettings.reverseSoundsOrder ? (mainSettings.moveLinesAmount - modIndex - 1) : modIndex
  }${
    mainSettings.instrument.pathEnd
  }`
}

function playSound(buffer) {
  const source = audioContext.createBufferSource(); // creates a sound source
  source.buffer = buffer; // tell the source which sound to play
  // source.connect(audioContext.destination); // connect the source to the audioContext's destination (the speakers)
  source.connect(gainNode) // now instead of connecting to audioContext.destination, connect to the gainNode, to change volume -> see https://stackoverflow.com/a/43386415
  source.start(); // play the source now
}


let soundEnabled = false;
updateButtonSoundToggle()
let soundEnabledOnce = false;

document.onvisibilitychange = () => toggleSound(false); // disable sound whenever window gets minimized, else sound will play all at once when you go back to the window, or the frame rendering will be off and the sound plays with weird timing. Not nice either way.
paper.onclick = toggleSound;
overlayCenter.onclick = toggleSound;
buttonSoundToggle.onclick = toggleSound;

function toggleSound(bool) {
  // initialize AudioContext, cannot be done before, browsers will block this as part of initially automatic running javascript
  if (audioContext === undefined) {
    initializeAudioContext();
  }
  soundEnabled = ((typeof bool) === "boolean") ? bool : !soundEnabled;
  updateButtonSoundToggle()
  if (soundEnabled && !soundEnabledOnce) {
    soundEnabledOnce = true;
    spanInfoToggleSound.style.display = "none";
  }
}
function updateButtonSoundToggle() {
  buttonSoundToggle.innerHTML = soundEnabled ? "🔊" : "🔇"
}


// some ui stuff
buttonVisibilityToggle.onclick = toggleUiVisibility;
let uiVisibile = true;
function toggleUiVisibility() {
  uiVisibile = !uiVisibile
  divUi.style.opacity = uiVisibile ? 1 : 0;
}

buttonAnimationToggle.onclick = toggleAnimationRunning;
let animationRunning = true;
let animationPausedAt;
function toggleAnimationRunning() {
  animationRunning = !animationRunning;
  buttonAnimationToggle.innerHTML = (
    animationRunning ?
    emojiPause :
    emojiPlay
  );
  if (!animationRunning) {
    animationPausedAt = getTimeNow();
  } else {
    const currentTime = getTimeNow();
    const timePassedSincePause = currentTime - animationPausedAt;
    startTimeMilli += timePassedSincePause;
    resetZeroTime();
    drawFrame(true);
  }
}

buttonRestartAnimation.onclick = restartAnimation;
function restartAnimation() {
  startTimeMilli = getTimeNow();
  resetZeroTime();
  elapsedStartSecondsFloored = -1; // -1 so that the slightly optimized update immediately increments to 0

  // reset impact times
  const newPaths = paths.map(path => {
    const previousImpactMillis = getPreviousImpactMillis(
      path.impactDelta,
      startTimeMilli,
      path.impactTimeShiftMillis
    );

    let newPath = {
      ...path,
      mostRecentImpactMillis: previousImpactMillis - (
        previousImpactMillis === 0 ?
        path.impactDelta :
        0
      )
    }

    if (variationSettings.variation === VARIATION_ENUM.dvd) {
      const previousImpactMillisY = getPreviousImpactMillis(
        path.impactDeltaY,
        startTimeMilli,
        path.impactTimeShiftMillisY
      );

      newPath = {
        ...newPath,
        mostRecentImpactMillisY: previousImpactMillisY - (
          previousImpactMillisY === 0 ?
          path.impactDelta :
          0
        ),
      }
    }

    return newPath
  })
  paths.splice(0, paths.length, ...newPaths)

  if (!animationRunning) {
    animationPausedAt = getTimeNow();
    currentTime = getTimeNow();
    drawFrame();
    if (timerVisible) {
      updateTimer((getTimeNow() - zeroTimeMilli) / 1000);
    }
  }
}

buttonTimerSinceStart.onclick = toggleTimerVisibility;
let timerVisible = true;
function toggleTimerVisibility() {
  timerVisible = !timerVisible
  spanTimer.style.display = timerVisible ? "unset" : "none";

  if (timerVisible && !animationRunning) {
    updateTimer((getTimeNow() - zeroTimeMilli) / 1000)
  }
}
toggleTimerVisibility() // disable timer at first

let elapsedStartSecondsFloored = -1; // -1 so that the slightly optimized update immediately increments to 0
function updateTimer(elapsedSecondsRelativeToZeroTime) {
  if (!timerVisible) {
    return;
  }
  const adjustedElapsedSeconds = elapsedSecondsRelativeToZeroTime - (startAnimationAtMilli / 1000);
  if (adjustedElapsedSeconds-1 > elapsedStartSecondsFloored) {
    elapsedStartSecondsFloored = Math.floor(adjustedElapsedSeconds);
    spanTimer.innerHTML = `${elapsedStartSecondsFloored >= 60 ? Math.floor(elapsedStartSecondsFloored / 60) + "m " : ""}${elapsedStartSecondsFloored % 60}s${startAnimationAtMilli === 0 ? "" : " (" + (startAnimationAtMilli > 0 ? "+" : "-") + (Math.abs(startAnimationAtMilli) / 1000) + "s)"}`;
  }
}


buttonChangeVariation.onclick = handleChangeVariation;
function handleChangeVariation() {
  variationSettings.variation = getArrayNextEntry(
    VARIATION_ENUMS.slice(0, -1), // TODO: include dvd variation
    variationSettings.variation
  );
  updateDimensions();
  updateCoordinates();
  if (!animationRunning) {
    drawFrame();
  }
}





// prepare paths unchanging stuff: color, audio, velocity, impact times
function generatePaths() {
  return generatePathHues(mainSettings.moveLinesAmount).map((hue, index) => {
    if (index >= currentAmountOfSoundBites) {
      console.warn("Warning, more moveLines than currentAmountOfSoundBites, so they probably have no sound.")
    }

    const numberOfPulsesPerFullAnimation = getNumberOfPulsesPerFullAnimation(index);
    const velocity = (singleWayTimeSeconds * numberOfPulsesPerFullAnimation) / durationSettings.fullAnimationTimeSeconds;

    const impactDelta = Math.abs(singleWayTimeMilli / velocity);

    const impactDistanceShift = getImpactDistanceShift();
    const impactTimeShiftMillis = getImpactTimeShiftMillis(velocity, impactDistanceShift);

    const previousImpactMillis = getPreviousImpactMillis(
      impactDelta,
      startTimeMilli,
      impactTimeShiftMillis
    );

    let path = {
      hue,
      audioBuffer: undefined,
      velocity,

      impactDistanceShift,
      impactDelta,
      mostRecentImpactMillis: previousImpactMillis,
      impactTimeShiftMillis,

      moveLinePositionAtTime: undefined,
      linearMoveLine: undefined,
      polygonMoveLinePositions: undefined,
      circleMoveLine: undefined,

      sphereShapeIndexNumberAdjusted: undefined,
      sphereShapeImpactNumber: undefined,
      sphereShapeImpactNumberAdjusted: undefined,

      distance: undefined, // updated each frame
      sphereCenter: undefined, // updated each frame
    }

    if (variationSettings.variation === VARIATION_ENUM.dvd) {
      const impactDeltaY = impactDelta / dvdYXMultiplier;

      const impactTimeShiftMillisY = getImpactTimeShiftMillis(velocity, impactDistanceShift);

      const previousImpactMillisY = getPreviousImpactMillis(
        impactDeltaY,
        startTimeMilli,
        impactTimeShiftMillisY
      );

      path = {
        ...path,
        impactDeltaY,
        mostRecentImpactMillisY: previousImpactMillisY,
        impactTimeShiftMillisY
      }
    }

    return path;
  });
}


function getImpactDistanceShift() {
  if (
    variationSettings.variation === VARIATION_ENUM.linear
    && variationSettings.variationLinearPulseAtLimit === false
  ) {
    return 0.5;
  }

  if (
    variationSettings.variation === VARIATION_ENUM.radial
    && (
      variationSettings.variationRadialCenterRadius === 0
      && !variationSettings.variationRadialBounceAtInner
      && variationSettings.variationRadialStartAtInside
    )
  ) {
    return 0.5;
  }

  return 0;
}


function getImpactTimeShiftMillis(velocity, impactDistanceShift) {
  return 1000 * impactDistanceShift / velocity;
}


function generatePathHues(amount) {
  const hue = styleSettings.moveLineColorHSLHue;
  if (hue === "colorGradient") {
    return generateHues(amount);
  } else if (hue === "colorGradientReverse") {
    return generateHues(amount, true);
  } else if (
    typeof hue !== "number"
    || hue < 0
    || hue > 360
  ) {
    return generateHues(amount).toReversed(); // default case
  } else {
    return Array(amount).fill(hue)
  }
}

function generateHues(amount, reverse) {
  const hues = [];
  const hueDelta = Math.trunc(360 / (amount -1)) // see https://mika-s.github.io/javascript/colors/hsl/2017/12/05/generating-random-colors-in-javascript.html

  for (let i = 0; i < amount; i++) {
    // const newHue = `hsla(${hueDelta * i}, 100%, 50%, 1.0)` // hsl color with hue depending on i, 100% saturation, 50% lightness (fulll color), 1.0 alpha (full opacity);
    hues.push(
      reverse ?
      (360 - hueDelta * i) :
      hueDelta * i
    );
  }

  return hues
}


function getNumberOfPulsesPerFullAnimation(index) {
  const first = durationSettings.pulsesAmountByFirstSphere;
  const number = durationSettings.pulsesDifferenceNumber;
  const style = durationSettings.pulsesDifferenceStyle;

  switch (style) {
    case "sub":
      return first - number * index;
    case "add":
      return first + number * index;
    case "div":
      return first / Math.pow(number, index);
    case "mult":
      return first * Math.pow(number, index);
    default:
      return first - number * index;
  }
}





const neutralRotation = 90; // upwards, take care of y positive going down in html, not up

/// some global variables that might not be changed every frame
let animationCenter;
let animationSize, animationRadius;
let currentTime;
let elapsedTimeSeconds;

let sphereWidth, sphereRadius, sphereOutlineWidth, pulseLineWidth, moveLineWidth, moveLinePulseWidthExtra, connectionLineWidth, boundingWidth;

// for variation mode linear
let linearHeight;
let linearWidth;
let linearBorderLeft;
let linearBorderRight;
let linearBorderTop;
let linearBorderBottom;
let linearBoundingPoints;

// for variation mode dvd // TODO implement
let dvdFrameHeight;
let dvdFrameWidth;
let dvdBorderLeft;
let dvdBorderRight;
let dvdBorderTop;
let dvdBorderBottom;
let dvdBoundingPoints;
let dvdYXMultiplier;

// for main movement stuff
let pulseLinesAtTime;

// bounding stuff
let boundingLines = {};
let boundingPositionAtTime; // only used for sphere connection with boundary


/// update the global variables
function updateDimensions() {
  paper.width = paper.clientWidth;
  paper.height = paper.clientHeight;

  animationSize = (Math.min(0.9 * paper.width, 0.8 * paper.height)) * styleSettings.animationSizeScale;
  animationRadius = animationSize / 2;
  animationCenter = {
    x: (
      (paper.width / 2)
      + styleSettings.animationShiftPosition.x * animationSize
    ),
    y: (
      (paper.height / 2)
      + styleSettings.animationShiftPosition.y * animationSize
    )
  };

  sphereWidth = styleSettings.sphereWidth * animationSize;
  sphereRadius = sphereWidth / 2;
  sphereOutlineWidth = styleSettings.sphereOutlineWidth * animationSize;
  pulseLineWidth = styleSettings.pulseLineWidth * animationSize;
  moveLineWidth = styleSettings.moveLineWidth * animationSize;
  moveLinePulseWidthExtra = styleSettings.moveLinePulseWidthExtra * moveLineWidth;
  connectionLineWidth = styleSettings.connectionLineWidth * animationSize;
  boundingWidth = styleSettings.boundingWidth * animationSize;

  if (variationSettings.variation === VARIATION_ENUM.linear) {
    linearHeight = paper.height * 0.8 * styleSettings.animationSizeScale;
    linearWidth = paper.width * 0.9 * styleSettings.animationSizeScale * variationSettings.variationLinearWidthScale;
    linearBorderLeft = animationCenter.x - (linearWidth / 2);
    linearBorderRight = animationCenter.x + (linearWidth / 2);
    linearBorderTop = animationCenter.y - (linearHeight / 2);
    linearBorderBottom = animationCenter.y + (linearHeight / 2);
    linearBoundingPoints = [
      { x: linearBorderLeft, y: linearBorderTop },
      { x: linearBorderRight, y: linearBorderTop },
      { x: linearBorderRight, y: linearBorderBottom },
      { x: linearBorderLeft, y: linearBorderBottom }
    ]
  }

  if (variationSettings.variation === VARIATION_ENUM.dvd) {
    const dvdHeight = variationSettings.variationDvdHeight;
    const dvdWidth = variationSettings.variationDvdWidth;
    const dvdDivisor = Math.max(dvdHeight, dvdWidth);

    const sphereSlopeX = variationSettings.variationDvdSphereSlopeX;
    const sphereSlopeY = variationSettings.variationDvdSphereSlopeY;

    const dvdHeightScale = variationSettings.variationDvdHeightScale;
    const dvdWidthScale = variationSettings.variationDvdWidthScale;
    const dvdScaleDivisor = Math.max(dvdHeightScale, dvdWidthScale);

    dvdYXMultiplier = (sphereSlopeY / sphereSlopeX) * (dvdWidth / dvdHeight);

    dvdFrameHeight = (
      animationSize
      * (dvdHeight / dvdDivisor)
      * (dvdHeightScale / dvdScaleDivisor)
    );
    dvdFrameWidth = (
      animationSize
      * (dvdWidth / dvdDivisor)
      * (dvdWidthScale / dvdScaleDivisor)
    );

    dvdBorderLeft = animationCenter.x - (dvdFrameWidth / 2);
    dvdBorderRight = animationCenter.x + (dvdFrameWidth / 2);
    dvdBorderTop = animationCenter.y - (dvdFrameHeight / 2);
    dvdBorderBottom = animationCenter.y + (dvdFrameHeight / 2);
    dvdBoundingPoints = [
      { x: dvdBorderLeft, y: dvdBorderTop },
      { x: dvdBorderRight, y: dvdBorderTop },
      { x: dvdBorderRight, y: dvdBorderBottom },
      { x: dvdBorderLeft, y: dvdBorderBottom }
    ]
  }
}

function updateCoordinates() {
  pulseLinesAtTime = updatePulseLines();
  updatePathMoveLines(paths, pulseLinesAtTime);
  boundingPositionAtTime = updateBoundingLines(pulseLinesAtTime);
}

function updateTime() {
  if (animationRunning) {
    currentTime = getTimeNow();
  }

  elapsedTimeSeconds = (currentTime - zeroTimeMilli) / 1000;
  updateTimer(elapsedTimeSeconds);
}

function updatePathsSphereShapeIndexNumbers() {
  if (
    styleSettings.sphereShape !== SPHERE_SHAPE_ENUM.indexNumber
    && styleSettings.sphereShape !== SPHERE_SHAPE_ENUM.indexNumberReverse
  ) {
    return;
  }

  paths.map((path, index) => {
    const baseNumber = (
      (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.indexNumber) ?
      index :
      paths.length - index - 1 // this is case styleSettings.sphereShape === SPHERE_SHAPE_ENUM.indexNumberReverse
    )
    path.sphereShapeIndexNumberAdjusted = getSphereShapeNumberAdjusted(baseNumber, index);
  });
}

function getSphereShapeNumberAdjusted(baseNumber, index) {
  const numAdd = styleSettings.sphereShapeNumberAdd;
  const numAddPerIndex = styleSettings.sphereShapeNumberAddPerIndex;
  const numMult = styleSettings.sphereShapeNumberMult;
  const numExpOf = styleSettings.sphereShapeNumberExpOf;

  const numberPrePow = (baseNumber + numAdd + (index * numAddPerIndex)) * numMult;
  return (
    (numExpOf === 0) ?
    (numberPrePow) :
    (Math.pow(numExpOf, numberPrePow))
  );
}





// call some of the update functions, maybe move somewhere else after ui settings implementation
updateDimensions(); // TODO maybe call if styling or dimension stuff changes
const paths = generatePaths();
updateCoordinates();
updatePathsSphereShapeIndexNumbers(); // TODO maybe call if sphereShape stuff gets changed


/// main loop
function drawFrame(keepDrawing) {
  updateTime();

  // in case of rescaling of the window
  if (paper.width !== paper.clientWidth || paper.height !== paper.clientHeight) {
    updateDimensions();
    updateCoordinates();
    if (!animationRunning) {
      drawFrame();
    }
  }

  pen.clearRect(0, 0, paper.width, paper.height); // clear the canvas. Previously the canvas was cleared by resetting paper.width, width "paper.width = paper.clientWidth", which is not very performant, see https://stackoverflow.com/a/6722031

  // draw pulse lines
  if (!styleSettings.pulseLinesAboveMoveLines) {
    drawPulseLines(pulseLinesAtTime);
  }

  // draw move line paths
  drawMoveLines(paths);

  if (styleSettings.pulseLinesAboveMoveLines) {
    drawPulseLines(pulseLinesAtTime);
  }

  // prepare sphere centers here to use for drawing and optionally connecting them
  updatePathsProgress(paths);

  // draw polygon delivery mode spheres
  drawPolygonDeliverySpheres(paths);

  drawSphereConnections(paths, boundingPositionAtTime);

  // draw circles
  drawSpheres(paths);

  drawBounding();

  // play circle sound if hit the baseLine
  updateImpactTimeAndPlayPulseSounds(paths)

  if (keepDrawing && animationRunning) {
    requestAnimationFrame(() => drawFrame(keepDrawing));
  }
}

drawFrame(true);



/// main functions
function updatePulseLines() { // these are just for drawing them, not for sound timing (which is done separately) // TODO CONTINUE remove polygon pulseLineAtTime time dependence, needs to rework the moveLine creation

  const pulseLineAtTimeArray = [];

  if (
    variationSettings.variation === VARIATION_ENUM.radial
    || variationSettings.variation === VARIATION_ENUM.dvd
  ) {
    return pulseLineAtTimeArray;
  }

  if (variationSettings.variation === VARIATION_ENUM.linear) {
    const amount = variationSettings.variationLinearPulseLinesAmount;
    const pulseAtLimit = variationSettings.variationLinearPulseAtLimit;
    const direction = variationSettings.variationLinearDirection;

    const heightDelta = linearHeight / amount;
    const widthDelta = linearWidth / amount;

    // TODO put this outside, more functional style
    for (let i = 0; i < amount + (pulseAtLimit ? 1 : 0); i++) {

      let start, end;

      if (direction === "right" || direction === "left") {
        const right = (direction === "right");
        const iX = (right ? linearBorderLeft : linearBorderRight) + (right ? 1 : -1) * (widthDelta * i + (pulseAtLimit ? 0 : (widthDelta / 2)))
        start = {
          x: iX,
          y: linearBorderTop
        }
        end = {
          x: iX,
          y: linearBorderBottom
        }
      } else {
        const top = (direction === "top");
        const iY = (top ? linearBorderBottom : linearBorderTop) + (top ? -1 : 1) * (heightDelta * i + (pulseAtLimit ? 0 : (heightDelta / 2)))
        start = {
          x: linearBorderLeft,
          y: iY
        }
        end = {
          x: linearBorderRight,
          y: iY
        }
      }

      pulseLineAtTimeArray.push({start, end});
    }

    return pulseLineAtTimeArray;
  }

  if (variationSettings.variation === VARIATION_ENUM.polygon) {
    const edges = variationSettings.variationPolygonEdgesAmount;
    const reverseDir = variationSettings.variationPolygonReverseDirection;
    const centerEmpty = variationSettings.variationPolygonCenterEmpty;
    const centerPulseLines = variationSettings.variationPolygonCenterPulseLinesExtend;
    const rotation = variationSettings.variationPolygonRotation;

    const degreesDelta = 360 / edges;
    const radius = animationRadius;
    const innerEmptyRadius = radius * centerEmpty;
    const centerEmptyDiff = 1 - centerEmpty;

    for (let i = 0; i < edges; i++) {
      const iRadians = degreeToRadians(- (neutralRotation + (reverseDir ? -1 : 1) * (rotation + degreesDelta * i))); // the starting minus sign is there to adjust for y positive values going down in html, instead of up

      const start = {
        x: animationCenter.x + (centerPulseLines ? 0 : (Math.cos(iRadians) * innerEmptyRadius)),
        y: animationCenter.y + (centerPulseLines ? 0 : (Math.sin(iRadians) * innerEmptyRadius)),
      };
      const end = {
        x: animationCenter.x + Math.cos(iRadians) * radius,
        y: animationCenter.y + Math.sin(iRadians) * radius
      };

      function pulseLineAtTime(time) {
        const moduloTime = (
          time === 1 ?
          1 :
          realMod(time, 1) // time loops after 1
        );

        if (centerPulseLines && centerEmpty > 0) {
          return {
            x: ((1 - centerEmptyDiff * moduloTime) - centerEmpty) * start.x + (centerEmptyDiff * moduloTime + centerEmpty) * end.x, // linear interpolation, with added empty center
            y: ((1 - centerEmptyDiff * moduloTime) - centerEmpty) * start.y + (centerEmptyDiff * moduloTime + centerEmpty) * end.y // linear interpolation, with added empty center
          };
        } else {
          return {
            x: (1 - moduloTime) * start.x + moduloTime * end.x, // linear interpolation
            y: (1 - moduloTime) * start.y + moduloTime * end.y // linear interpolation
          };
        }
      }

      pulseLineAtTimeArray.push(pulseLineAtTime);
    }

    return pulseLineAtTimeArray;
  }

  if (variationSettings.variation === VARIATION_ENUM.circle) {
    const amount = variationSettings.variationCirclePulseLinesAmount;
    const arcDegrees = variationSettings.variationCircleArcDegrees;
    const centerEmpty = variationSettings.variationCircleCenterEmpty;
    const centerPulseLines = variationSettings.variationCircleCenterPulseLinesExtend;
    const rotation = variationSettings.variationCircleRotation;

    const bounceOffset = variationSettings.variationCircleBounceOffset;

    const radius = animationRadius;
    const innerEmptyRadius = radius * centerEmpty;

    const degreeDelta = arcDegrees / amount;
    const startAngle = neutralRotation - (arcDegrees / 2) + rotation;

    for (let i = 0; i < ((arcDegrees % 360) === 0 ? amount : (amount + 1)); i++) {
      const angle = degreeToRadians(- (startAngle + i * degreeDelta)); // the starting minus sign is there to adjust for y positive values going down in html, instead of up

      const bounceOffsetDistance = (
        bounceOffset ?
        sphereRadius :
        0
      );

      start = {
        x: animationCenter.x + (centerPulseLines ? 0 : (Math.cos(angle) * innerEmptyRadius)),
        y: animationCenter.y + (centerPulseLines ? 0 : (Math.sin(angle) * innerEmptyRadius)) + bounceOffsetDistance,
      };
      end = {
        x: animationCenter.x + Math.cos(angle) * radius,
        y: animationCenter.y + Math.sin(angle) * radius + bounceOffsetDistance
      };

      pulseLineAtTimeArray.push({start, end});
    }

    return pulseLineAtTimeArray;
  }
}


function linearAngleAtAnimationTime(elAnimationTime, rotation, rotationSpeed) {
  const currentRotationFromRotationSpeed = rotationSpeed * (elAnimationTime / durationSettings.fullAnimationTimeSeconds) * (360 / amount);

  return degreeToRadians(- (rotation + currentRotationFromRotationSpeed)); // the starting minus sign is there to adjust for y positive values going down in html, instead of up
}


function updatePathMoveLines(paths, pulseLinesAtTime) {
  const amount = paths.length;

  if (variationSettings.variation === VARIATION_ENUM.linear) {
    const pulsesAmount = variationSettings.variationLinearPulseLinesAmount;
    const moveLinesAtLimit = variationSettings.variationLinearMoveLinesAtLimit;
    const direction = variationSettings.variationLinearDirection;
    const bounce = variationSettings.variationLinearBounce;

    const bounceAdjustedPulseAmount = bounce ? (pulsesAmount * 2) : pulsesAmount;

    const heightDelta = linearHeight / (amount + (moveLinesAtLimit ? -1 : 1));
    const widthDelta = linearWidth / (amount + (moveLinesAtLimit ? -1 : 1));

    paths.map((path, idx) => {
      const index = (
        mainSettings.reverseMoveLinesOrder ?
        (amount - idx - 1) :
        idx
      );

      let start, end;

      if (direction === "right" || direction === "left") {
        const right = (direction === "right");
        const iY = linearBorderTop + heightDelta * (index + (moveLinesAtLimit ? 0 : 1))
        start = {
          x: (right ? linearBorderLeft : linearBorderRight),
          y: iY
        }
        end = {
          x: (right ? linearBorderRight : linearBorderLeft),
          y: iY
        }
      } else {
        const top = (direction === "top");
        const iX = linearBorderLeft + widthDelta * (index + (moveLinesAtLimit ? 0 : 1))
        start = {
          x: iX,
          y: (top ? linearBorderBottom : linearBorderTop)
        }
        end = {
          x: iX,
          y: (top ? linearBorderTop : linearBorderBottom)
        }
      }

      path.linearMoveLine = { start, end };

      function moveLinePositionAtTime(time) { // time in seconds, as should always be assumed in this spaghetti code if it does not say "Milli"
        const moduloTime = (
          time === bounceAdjustedPulseAmount ?
          bounceAdjustedPulseAmount :
          realMod(
            durationSettings.generalAnimationSpeedMultiplier * time,
            bounceAdjustedPulseAmount
          )
        );

        let normedModuloTime;
        if (bounce) {
          normedModuloTime = 2 * moduloTime / bounceAdjustedPulseAmount;
          normedModuloTime = (
            bounce ?
            (normedModuloTime > 1 ? (2 - normedModuloTime) : normedModuloTime) :
            normedModuloTime
          );
        } else {
          normedModuloTime = moduloTime / bounceAdjustedPulseAmount;
        }

        return {
          x: (1 - normedModuloTime) * start.x + normedModuloTime * end.x, // linear interpolation
          y: (1 - normedModuloTime) * start.y + normedModuloTime * end.y, // linear interpolation
        }
      }

      path.moveLinePositionAtTime = moveLinePositionAtTime;
    });

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.polygon) {
    const bounce = variationSettings.variationPolygonBounce;
    const edges = variationSettings.variationPolygonEdgesAmount;
    const bounceAdjustedEdges = bounce ? (edges * 2) : edges;

    const divisor = variationSettings.variationPolygonPulseLineDivisor || 1;

    const moveLinesAtLimit = variationSettings.variationPolygonMoveLinesAtLimit;

    paths.map((path, idx) => {
      const index = (
        mainSettings.reverseMoveLinesOrder ?
        (amount - idx - 1) :
        idx
      );

      const positions = pulseLinesAtTime.map(pulseLineAtTime => {
        if (moveLinesAtLimit) {
          return pulseLineAtTime(index / (amount - 1));
        } else {
          return pulseLineAtTime((index + 1) / (amount + 1));
        }
      });

      path.polygonMoveLinePositions = positions;

      function moveLinePositionAtTime(time) { // time in seconds, as should always be assumed in this spaghetti code if it does not say "Milli"
        const moduloTime = realMod(
          durationSettings.generalAnimationSpeedMultiplier * time,
          bounceAdjustedEdges
        );
        const flooredModTime = Math.floor(moduloTime);
        const ceiledModTime = (flooredModTime + 1) % bounceAdjustedEdges;
        const modOneTime = moduloTime % 1;

        const bounceAdjustedFloored = (
          (bounce && flooredModTime >= edges) ?
          realMod(-flooredModTime, edges) :
          flooredModTime
        );
        const bounceAdjustedCeiled = (
          (bounce && ceiledModTime >= edges) ?
          realMod(-ceiledModTime, edges) :
          ceiledModTime
        );

        return {
          x: (1 - modOneTime) * positions[bounceAdjustedFloored].x + modOneTime * positions[bounceAdjustedCeiled].x, // linear interpolation
          y: (1 - modOneTime) * positions[bounceAdjustedFloored].y + modOneTime * positions[bounceAdjustedCeiled].y // linear interpolation
        }
      }

      path.moveLinePositionAtTime = ((t) => moveLinePositionAtTime(t * divisor));
    });

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.circle) {
    const pulsesAmount = variationSettings.variationCirclePulseLinesAmount;
    const arcDegrees = variationSettings.variationCircleArcDegrees
    const moveLinesAtLimit = variationSettings.variationCircleMoveLinesAtLimit;
    const reverseDir = variationSettings.variationCircleStartAtOtherEnd;
    const centerEmpty = variationSettings.variationCircleCenterEmpty;

    const bounce = variationSettings.variationCircleBounce;

    const rotation = variationSettings.variationCircleRotation;

    const radius = animationRadius;
    const innerEmptyRadius = radius * centerEmpty;

    const startAngle = neutralRotation - (arcDegrees / 2) + rotation;
    const endAngle = neutralRotation + (arcDegrees / 2) + rotation;
    const startRadians = degreeToRadians(startAngle);
    const endRadians = degreeToRadians(endAngle);
    const radiansDelta = (endRadians - startRadians) / pulsesAmount;

    const bounceAdjustedPulseAmount = bounce ? (pulsesAmount * 2) : pulsesAmount;

    const radiusDelta = (radius - innerEmptyRadius) / (amount + (moveLinesAtLimit ? -1 : 1));
    const firstRadius = innerEmptyRadius + (moveLinesAtLimit ? 0 : 1) * radiusDelta;

    paths.map((path, idx) => {
      const index = (
        mainSettings.reverseMoveLinesOrder ?
        (amount - idx - 1) :
        idx
      );

      const pathRadius = firstRadius + index * radiusDelta;

      path.circleMoveLine = { radius: pathRadius, startRadians, endRadians };

      function moveLinePositionAtTime(time) { // time in seconds, as should always be assumed in this spaghetti code if it does not say "Milli"
        let moduloTime = (
          time === bounceAdjustedPulseAmount ?
          bounceAdjustedPulseAmount :
          realMod(
            durationSettings.generalAnimationSpeedMultiplier * time,
            bounceAdjustedPulseAmount
          )
        );

        moduloTime = ( // adjust for bounce
          bounce && (moduloTime > pulsesAmount) ?
          (2 * pulsesAmount - moduloTime) :
          moduloTime
        );

        moduloTime = (
          reverseDir ?
          pulsesAmount - moduloTime :
          moduloTime
        );

        const currentRadians = startRadians + moduloTime * radiansDelta;

        return {
          x: animationCenter.x + pathRadius * Math.cos(currentRadians),
          y: animationCenter.y - pathRadius * Math.sin(currentRadians)
        }
      }

      path.moveLinePositionAtTime = moveLinePositionAtTime;
    });

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.radial) {
    const arcDegrees = variationSettings.variationRadialArcDegrees
    const distributeFromCenter = variationSettings.variationRadialDistributionFromCenter;
    const startAtInside = variationSettings.variationRadialStartAtInside;
    const centerRadius = variationSettings.variationRadialCenterRadius;
    const bounceCenter = (
      centerRadius !== 0
      || variationSettings.variationRadialBounceAtInner
    );
    const adjustBounce = variationSettings.variationRadialAdjustBounceBySphereWidth;

    const rotation = variationSettings.variationRadialRotation;

    const outerRadius = animationRadius;
    const innerRadius = outerRadius * centerRadius;

    const neutralRadians = degreeToRadians(neutralRotation);
    const startAngle = neutralRotation - (arcDegrees / 2) + rotation;
    const endAngle = neutralRotation + (arcDegrees / 2) + rotation;
    const startRadians = degreeToRadians(startAngle);
    const endRadians = degreeToRadians(endAngle);
    const radiansDelta = (endRadians - startRadians) / paths.length;

    const adjustedInnerDistance = innerRadius + ((bounceCenter && adjustBounce) ? sphereRadius : 0);
    const adjustedOuterDistance = outerRadius - (adjustBounce ? sphereRadius : 0);

    const totalMoveLineTime = 2; // just lazily leave this one here

    paths.map((path, idx) => {
      const index = (
        mainSettings.reverseMoveLinesOrder ?
        (amount - idx - 1) :
        idx
      );

      let currentRadians;

      if (distributeFromCenter) {
        currentRadians = neutralRadians + (
          index % 2 === 0 ? // is even
          (index / 2) * radiansDelta : // if even
          - (index + 1) / 2 * radiansDelta // if odd
        );
      } else {
        currentRadians = startRadians + index * radiansDelta;
      }


      const currentRadiansCos = Math.cos(currentRadians);
      const currentRadiansSin = Math.sin(currentRadians);

      const inner = {
        x: animationCenter.x + adjustedInnerDistance * currentRadiansCos,
        y: animationCenter.y - adjustedInnerDistance * currentRadiansSin,
      };
      const outer = {
        x: animationCenter.x + adjustedOuterDistance * currentRadiansCos,
        y: animationCenter.y - adjustedOuterDistance * currentRadiansSin,
      };
      const outerMirrored = {
        x: animationCenter.x - adjustedOuterDistance * currentRadiansCos,
        y: animationCenter.y + adjustedOuterDistance * currentRadiansSin,
      };

      path.radialMoveLine = (
        bounceCenter ?
        { start: inner, end: outer } :
        { start: outerMirrored, end: outer }
      );

      function moveLinePositionAtTime(time) {
        let moduloTime = bounceMod(
          durationSettings.generalAnimationSpeedMultiplier * time - (startAtInside ? (totalMoveLineTime / (bounceCenter ? 2 : 4)) : 0),
          totalMoveLineTime / 2
        );

        return {
          x: (1 - moduloTime) * outer.x + moduloTime * (bounceCenter ? inner : outerMirrored).x,
          y: (1 - moduloTime) * outer.y + moduloTime * (bounceCenter ? inner : outerMirrored).y
        }
      }

      path.moveLinePositionAtTime = moveLinePositionAtTime;
    });

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.dvd) {
    const bounce = variationSettings.variationDvdBounce;

    const distanceXPerSecond = 1; // if this is changed, the pulse calculation (for visual effects and sound playing) needs to be redone. There we always assume that the time it takes between pulses is 1 second
    const distanceYPerSecond = dvdYXMultiplier * distanceXPerSecond;

    // width and height distance are each a value from 0 to 1, so progress in these directions is relative

    paths.map((path, idx) => {
      const index = (
        mainSettings.reverseMoveLinesOrder ?
        (amount - idx - 1) :
        idx
      );

      function moveLinePositionAtTime(time) {
        const distanceX = distanceXPerSecond * durationSettings.generalAnimationSpeedMultiplier * time;
        const distanceY = distanceYPerSecond * durationSettings.generalAnimationSpeedMultiplier * time;

        let moduloDistanceX;
        let moduloDistanceY;

        if (bounce) {
          moduloDistanceX = bounceMod(distanceX, 1);
          moduloDistanceY = bounceMod(distanceY, 1);
        } else {
          moduloDistanceX = realMod(distanceX, 1);
          moduloDistanceY = realMod(distanceY, 1);
        }

        return {
          x: dvdBorderLeft + moduloDistanceX * dvdFrameWidth,
          y: dvdBorderBottom - moduloDistanceY * dvdFrameHeight
        }
      }

      path.moveLinePositionAtTime = moveLinePositionAtTime;
    });

    return;
  }
}


function drawMoveLines(paths) {
  if (!styleSettings.moveLinesVisible) {
    return;
  }
  pen.lineCap = "round";

  if (variationSettings.variation === VARIATION_ENUM.linear) {
    paths.map(path => {
      handleMoveLineColor(path, elapsedTimeSeconds)
      drawLine(path.linearMoveLine.start, path.linearMoveLine.end);
    })

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.polygon) {
    paths.map(path => {
      handleMoveLineColor(path, elapsedTimeSeconds)
      drawPolygon(path.polygonMoveLinePositions, true, false);
    });

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.circle) {
    paths.map(path => {
      handleMoveLineColor(path, elapsedTimeSeconds)
      drawArc(
        animationCenter,
        path.circleMoveLine.radius,
        path.circleMoveLine.startRadians,
        path.circleMoveLine.endRadians
      );
    })

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.radial) {
    paths.map(path => {
      handleMoveLineColor(path, elapsedTimeSeconds)
      drawLine(path.radialMoveLine.start, path.radialMoveLine.end);
    })

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.dvd) {
    return;
  }
}

function drawPulseLines(pulseLines) {
  pen.strokeStyle = styleSettings.pulseLineColor;
  pen.lineWidth = pulseLineWidth;
  pen.lineCap = "round"; // butt (default), round, square

  if (!styleSettings.pulseLinesVisible) {
    return;
  }

  function pulseVisibleForIndex(index) {
    if (variationSettings.variation === VARIATION_ENUM.polygon) {
      const divisor = variationSettings.variationPolygonPulseLineDivisor || 1;
      const divisorShift = variationSettings.variationPolygonPulseLinesShift;

      return realMod(index, divisor) === realMod(divisorShift, divisor);

    } else {
      return true;
    }
  }

  if (
    variationSettings.variation === VARIATION_ENUM.linear
    ||  variationSettings.variation === VARIATION_ENUM.circle
  ) {
    pulseLines.map(pulseLine => {
      drawLine(pulseLine.start, pulseLine.end);
    });
  } else if (variationSettings.variation === VARIATION_ENUM.polygon) {
    pulseLines.map((pulseLine, index) => {
      if (pulseVisibleForIndex(index)) {
        drawLine(pulseLine(0), pulseLine(1));
      }
    });
  }
}


function updateBoundingLines(pulseLines) {

  if (variationSettings.variation === VARIATION_ENUM.linear) {
    boundingLines.positions = linearBoundingPoints;
    return getPolygonAtTimeFunction(linearBoundingPoints, 1);
  }

  if (variationSettings.variation === VARIATION_ENUM.dvd) {
    const shiftX = sphereRadius; // TODO implement sphereShape that has width different from height
    const shiftY = sphereRadius;
    boundingLines.positions = (
      variationSettings.variationDvdBounceBoundingSizeAdjustment ?
      [
        { x: dvdBorderLeft - shiftX, y: dvdBorderTop - shiftY },
        { x: dvdBorderRight + shiftX, y: dvdBorderTop - shiftY },
        { x: dvdBorderRight + shiftX, y: dvdBorderBottom + shiftY },
        { x: dvdBorderLeft - shiftX, y: dvdBorderBottom + shiftY }
      ] :
      dvdBoundingPoints
    );
    return getPolygonAtTimeFunction(boundingLines.positions, 1);
  }

  if (boundingWidth === 0) {
    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.polygon) {
    if (variationSettings.variationPolygonBoundingOuter) {
      boundingLines.outer = pulseLines.map(pulseLine => pulseLine(1));
    }

    if (
      variationSettings.variationPolygonBoundingInner
      && variationSettings.variationPolygonCenterEmpty !== 0
    ) {
      boundingLines.inner = pulseLines.map(pulseLine => pulseLine(0));
    }

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.circle) {
    if (
      variationSettings.variationCircleBoundingOuter === false
      && variationSettings.variationCircleBoundingInner === false
    ) {
      return
    }

    const radius = animationRadius;
    const arcDegrees = variationSettings.variationCircleArcDegrees;
    const rotation = variationSettings.variationCircleRotation;
    const startAngle = neutralRotation - (arcDegrees / 2) + rotation;
    const endAngle = neutralRotation + (arcDegrees / 2) + rotation;
    const startRadians = degreeToRadians(startAngle);
    const endRadians = degreeToRadians(endAngle);

    if (variationSettings.variationCircleBoundingOuter) {
      boundingLines.outer = { radius, startRadians, endRadians }
    }

    if (variationSettings.variationCircleBoundingInner) {
      const centerEmpty = variationSettings.variationCircleCenterEmpty;
      const innerEmptyRadius = radius * centerEmpty;
      boundingLines.inner = { radius: innerEmptyRadius, startRadians, endRadians }
    }

    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.radial) {
    if (
      variationSettings.variationRadialBoundingOuter === false
      && variationSettings.variationRadialBoundingInner === false
    ) {
      return
    }

    const outerRadius = animationRadius;

    if (variationSettings.variationRadialBoundingOuter) {
      boundingLines.outerRadius = outerRadius;
    }

    if (variationSettings.variationRadialBoundingInner) {
      const innerRadius = variationSettings.variationRadialCenterRadius * outerRadius;
      boundingLines.innerRadius = innerRadius;
    }

    return;
  }
}

function drawBounding() {
  if (boundingWidth === 0) {
    return;
  }

  pen.strokeStyle = styleSettings.boundingColor;
  pen.fillStyle = styleSettings.boundingInnerFill;
  pen.lineWidth = boundingWidth;
  // pen.lineCap = "round";
  pen.lineJoin = "miter"; // round, bevel, miter (default)

  if (
    (
      variationSettings.variation === VARIATION_ENUM.linear
      && variationSettings.variationLinearBoundingBox
    ) || (
      variationSettings.variation === VARIATION_ENUM.dvd
      && variationSettings.variationDvdBoundingBox
    )
  ) {
    drawPolygon(boundingLines.positions, true, false);
  }

  if (variationSettings.variation === VARIATION_ENUM.polygon) {
    if (variationSettings.variationPolygonBoundingOuter) {
      drawPolygon(boundingLines.outer, true, false)
    }
    if (
      variationSettings.variationPolygonBoundingInner
      && variationSettings.variationPolygonCenterEmpty !== 0
    ) {
      drawPolygon(boundingLines.inner, true, true)
    }
    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.circle) {
    if (variationSettings.variationCircleBoundingOuter) {
      drawArc(
        animationCenter,
        boundingLines.outer.radius,
        boundingLines.outer.startRadians,
        boundingLines.outer.endRadians
      );
    }
    if (variationSettings.variationCircleBoundingInner) {
      drawArc(
        animationCenter,
        boundingLines.inner.radius,
        boundingLines.inner.startRadians,
        boundingLines.inner.endRadians,
        true
      );
    }
    return;
  }

  if (variationSettings.variation === VARIATION_ENUM.radial) { // TODO fix
    if (variationSettings.variationRadialBoundingOuter) {
      drawArc(animationCenter, boundingLines.outerRadius, 0, Math.PI * 2);
    }
    if (variationSettings.variationRadialBoundingInner) {
      drawArc(animationCenter, boundingLines.innerRadius, 0, Math.PI * 2, true);
    }
    return;
  }
}


function handleMoveLineColor(currentPath, elapsedTimeSeconds) { // handle visual line pulse
  const pulseNumber = ((1000*elapsedTimeSeconds - currentPath.mostRecentImpactMillis) / styleSettings.fadeOutImpactPulseTimeMilli);
  const pulseStrength = (pulseNumber >= 0 && pulseNumber <= 1) ? (1 - pulseNumber) : 0;

  pen.lineWidth = moveLineWidth + pulseStrength * moveLinePulseWidthExtra;

  const opacity = pulseStrength * styleSettings.moveLinePulseOpacity + (1 - pulseStrength) * styleSettings.moveLineSilenceOpacity // linear interpolation
  const lightness = pulseStrength * styleSettings.moveLinePulseLightness + (1 - pulseStrength) * styleSettings.moveLineSilenceLightness // linear interpolation

  pen.strokeStyle = `hsla(${currentPath.hue}, ${styleSettings.moveLineColorHSLSaturation * 100}%, ${lightness * 100}%, ${opacity})` // hsl color with hue depending on i, 100% saturation, 50% lightness (full color), last one alpha between 0.0 and 1.0 (full transparency to full opacity);
}


function drawPolygonDeliverySpheres(paths) {
  // draw polygon delivery mode spheres that are on the path of this current sphere // currently ignores the value of singleWayTimeMilli and just assumes that it is 1000, which it should probably be forever
  if (
    variationSettings.variation === VARIATION_ENUM.polygon
    && variationSettings.variationPolygonDelivery
  ) {
    const divisor = variationSettings.variationPolygonPulseLineDivisor || 1;
    const divisorShift = variationSettings.variationPolygonPulseLinesShift;
    const deliveryIgnoreDivisors = variationSettings.variationPolygonDeliveryIgnoreDivisors;

    const edgesAmount = variationSettings.variationPolygonEdgesAmount;
    const pulsesAmount = edgesAmount / divisor;
    const bounce = variationSettings.variationPolygonBounce;
    const inverted = variationSettings.variationPolygonDeliveryInverted;
    paths.map(currentPath => {
      const moveLinePositionAtTime = currentPath.moveLinePositionAtTime;
      const distance = currentPath.distance;

      for (let j = 0; j < edgesAmount; j++) {
        const adjustedJterator = j / divisor;

        if (!(
          deliveryIgnoreDivisors
          || realMod(j, divisor) === realMod(divisorShift, divisor)
        )) {
          continue;
        }

        const modDistance = realMod(distance, 2 * pulsesAmount);

        const visible = (
          bounce ?
          (
            modDistance < adjustedJterator || (modDistance > pulsesAmount && modDistance > realMod(-adjustedJterator, 2*pulsesAmount))
          ) : (
            modDistance < adjustedJterator || modDistance > pulsesAmount + adjustedJterator
          )
        )
        const invertedAdjustedVisible = (inverted ? !visible : visible)

        if (invertedAdjustedVisible) {
          const center = moveLinePositionAtTime(adjustedJterator);
          pen.beginPath();
          pen.fillStyle = styleSettings.sphereColor;
          pen.arc(center.x, center.y, sphereRadius / 2, 0, 2 * Math.PI);
          pen.fill();
        }
      }
    });
  }
}


function updatePathsProgress(paths) {
  return paths.map(path => {
    const moveLinePositionAtTime = path.moveLinePositionAtTime;

    path.distance = path.velocity * elapsedTimeSeconds;

    // save sphere centers
    path.sphereCenter = moveLinePositionAtTime(path.distance);
  })
}


function drawSphereConnections(paths, boundingPositionAtTime) {
  // draw lines that connect spheres
  const sphereConnections = styleSettings.connectionLineSpheres;
  if (
    sphereConnections === "center"
    || sphereConnections === "neighbors"
    || sphereConnections === "neighborsMod"
    || sphereConnections === "spheres"
    || sphereConnections === "centerAndNeighbors"
    || sphereConnections === "centerAndNeighborsMod"
    || (typeof sphereConnections) === "number"
    || isArrayOfType(sphereConnections, "number")
  ) {
    pen.strokeStyle = styleSettings.connectionLineColor;
    pen.lineWidth = connectionLineWidth;

    if (
      sphereConnections === "center"
      || sphereConnections === "centerAndNeighbors"
    ) {
      paths.map(path => {
        drawLine(animationCenter, path.sphereCenter);
      });
    }

    if (
      sphereConnections === "neighbors"
      || sphereConnections === "centerAndNeighbors"
      || sphereConnections === "neighborsMod"
      || sphereConnections === "centerAndNeighborsMod"
    ) {
      for (let i = 0; i < paths.length - 1; i++) {
        drawLine(paths[i].sphereCenter, paths[i + 1].sphereCenter);
      }
    }

    if (
      sphereConnections === "neighborsMod"
      || sphereConnections === "centerAndNeighborsMod"
    ) {
      drawLine(paths[paths.length - 1].sphereCenter, paths[0].sphereCenter);
    }

    if (sphereConnections === "spheres") {
      for (let i = 0; i < paths.length - 1; i++) {
        // WARNING: be careful with the n^2 many calculations needed here
        for (let j = i+1; j < paths.length; j++) {
          drawLine(paths[i].sphereCenter, paths[j].sphereCenter);
        }
      }
    }

    if ((typeof sphereConnections) === "number") {
      paths.map(path => {
        drawLine(path.moveLinePositionAtTime(sphereConnections), path.sphereCenter);
      });
    }

    if (
      Array.isArray(sphereConnections)
      && sphereConnections.every(el => (typeof el) === "number")
    ) {
      if (
        variationSettings.variation === VARIATION_ENUM.linear
      ) {
        paths.map(path => {
          sphereConnections.map(connectionPosition => {
            drawLine(boundingPositionAtTime(connectionPosition), path.sphereCenter)
          })
        })
      }
    }
  }
}


function drawSpheres(paths) {
  pen.fillStyle = styleSettings.sphereColor;
  pen.strokeStyle = styleSettings.sphereOutlineColor;
  pen.lineWidth = sphereOutlineWidth;
  pen.lineJoin = "round";

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.circle) {
    paths.map(path => {
      pen.beginPath();
      pen.arc(path.sphereCenter.x, path.sphereCenter.y, sphereRadius, 0, 2 * Math.PI);
      pen.fill();
    });
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.square) {
    paths.map(path => {
      pen.beginPath();
      pen.rect(path.sphereCenter.x - sphereRadius, path.sphereCenter.y - sphereRadius, sphereWidth, sphereWidth);
      pen.fill();
    });
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.diamond) {
    paths.map(path => {
      const centerToEdge = sqrtTwoAppr * sphereRadius
      points = [
        { x: path.sphereCenter.x, y: path.sphereCenter.y + centerToEdge},
        { x: path.sphereCenter.x + centerToEdge, y: path.sphereCenter.y},
        { x: path.sphereCenter.x, y: path.sphereCenter.y - centerToEdge},
        { x: path.sphereCenter.x - centerToEdge, y: path.sphereCenter.y}
      ]
      drawPolygon(points, false, true);
    });
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.triangle) {
    paths.map(path => {
      const distance = sphereWidth * sqrtThreeApprDivByFour;
      points = [
        { x: path.sphereCenter.x, y: path.sphereCenter.y - distance},
        { x: path.sphereCenter.x + sphereRadius, y: path.sphereCenter.y + distance},
        { x: path.sphereCenter.x - sphereRadius, y: path.sphereCenter.y + distance}
      ]
      drawPolygon(points, false, true);
    });
    return;
  }

  // only spheres with text from here
  pen.textAlign = "center";
  pen.font = (sphereWidth) + "px arial";

  if ((typeof styleSettings.sphereShape) === "string") {
    drawTextAtPositions(
      paths.map(path => path.sphereCenter),
      (2/3) * sphereRadius,
      styleSettings.sphereShape,
      true,
      sphereOutlineWidth !== 0
    )
    return;
  }

  if (isArrayOfType(styleSettings.sphereShape, "string")) {
    paths.map((path, index) => {
      drawTextAtPosition(
        path.sphereCenter,
        (2/3) * sphereRadius,
        styleSettings.sphereShape[index % styleSettings.sphereShape.length],
        true,
        sphereOutlineWidth !== 0
      );
    });
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.heart) { // ♥
    drawTextAtPositions(
      paths.map(path => path.sphereCenter),
      (2/3) * sphereRadius,
      "♥",
      true,
      sphereOutlineWidth !== 0
    )
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.indexNumber) {
    paths.map(path => {
      drawTextAtPosition(
        path.sphereCenter,
        (2/3) * sphereRadius,
        path.sphereShapeIndexNumberAdjusted,
        true,
        sphereOutlineWidth !== 0
      );
    });
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.indexNumberReverse) {
    paths.map(path => {
      drawTextAtPosition(
        path.sphereCenter,
        (2/3) * sphereRadius,
        path.sphereShapeIndexNumberAdjusted,
        true,
        sphereOutlineWidth !== 0
      );
    });
    return;
  }

  if (styleSettings.sphereShape === SPHERE_SHAPE_ENUM.numberOfImpacts) {
    paths.map((path, index) => {
      const newImpactNumber = Math.floor(path.distance + path.impactDistanceShift);
      if (path.sphereShapeImpactNumber !== newImpactNumber) {
        // save values so they don't need to be recalculated every frame
        path.sphereShapeImpactNumber = newImpactNumber;
        path.sphereShapeImpactNumberAdjusted = getSphereShapeNumberAdjusted(
          path.sphereShapeImpactNumber, index
        );
      }
      drawTextAtPosition(
        path.sphereCenter,
        (2/3) * sphereRadius,
        path.sphereShapeImpactNumberAdjusted,
        true,
        sphereOutlineWidth !== 0
      );
    });
    return;
  }
}

function drawTextAtPositions(positions, yOffset, text, fill, stroke) {
  positions.map(position => {
    drawTextAtPosition(position, yOffset, text, fill, stroke);
  });
}

function drawTextAtPosition(position, yOffset, text, fill, stroke) {
  if (fill) {
    pen.fillText(text, position.x, position.y + yOffset); // need to offset the y-coordinate by yOffset or else the text will be too high
  }
  if (stroke) {
    pen.fillText(text, position.x, position.y + yOffset);
  }
}


function updateImpactTimeAndPlayPulseSounds(paths) {
  if (!animationRunning) {
    return;
  }
  paths.map(currentPath => {
    const previousImpactMillis = getPreviousImpactMillis(
      currentPath.impactDelta,
      currentTime,
      currentPath.impactTimeShiftMillis
    ); // here "previous" means "the one that should have previously happened, so now is the time to catch up on it, if it is after the actually performed mostRecentImpact!"

    if (previousImpactMillis > currentPath.mostRecentImpactMillis) {
      if (soundEnabled) {
        playSound(currentPath.audioBuffer);
      }
      currentPath.mostRecentImpactMillis = previousImpactMillis;
    }

    if (variationSettings.variation === VARIATION_ENUM.dvd) {
      const previousImpactMillisY = getPreviousImpactMillis(
        currentPath.impactDeltaY,
        currentTime,
        currentPath.impactTimeShiftMillisY
      );

      if (previousImpactMillisY > currentPath.mostRecentImpactMillisY) {
        if (soundEnabled) {
          playSound(currentPath.audioBuffer);
        }
        currentPath.mostRecentImpactMillisY = previousImpactMillisY;
      }
    }
  })
}

function getPreviousImpactMillis(impactDelta, currentTime, impactShift) {
  let timeOfImpact = zeroTimeMilli + (impactShift ? impactShift : 0);

  if (variationSettings.variation === VARIATION_ENUM.polygon) {
    const divisor = variationSettings.variationPolygonPulseLineDivisor || 1;
    const divisorShift = variationSettings.variationPolygonPulseLinesShift;

    timeOfImpact = zeroTimeMilli + divisorShift * (impactDelta / divisor);
  }

  const impactMod = realMod(
    currentTime - timeOfImpact,
    impactDelta
  );
  const previousImpactTime = currentTime - impactMod;
  return previousImpactTime - zeroTimeMilli;
}



/// handle keyboard shortcuts
function handleKeyDown(event) {
  switch (event.key.toLowerCase()) {
    case ("v"):
      toggleUiVisibility();
      return;
    case ("m"):
      toggleSound();
      return;
    case (" "):
      event.preventDefault(); // else currently focused button will be pressed
      toggleAnimationRunning();
      return;
    case ("r"):
      restartAnimation();
      return;
    case ("t"):
      toggleTimerVisibility();
      return;
    case ("s"):
      handleChangeVariation();
      return;
    default:
      return;
  }
}




/// helpers
function realMod(number, modulus) {
  return ((number % modulus) + modulus) % modulus;
}
function bounceMod(number, modulus) { // returns a bouncing modulus, i.e for modulus 1 this returns 0, 0.2, 0.4, 0.6, 0.8, 1.0, 0.8, 0.6, 0.4, 0.2, 0, 0.2, ...
  const mod = realMod(number, 2 * modulus);
  return (
    mod > modulus ?
    2 * modulus - mod :
    mod
  )
}

function isArrayOfType(array, type) {
  return (
    Array.isArray(array)
    && array.every(el => (typeof el) === type)
  )
}

function getArrayNextEntry(array, current) {
  const currentIndex = array.indexOf(current);
  const index = (currentIndex === -1) ? 0 : currentIndex;
  const next = array[(index + 1) % array.length];
  return next;
}

function degreeToRadians(degree) {
  return 2 * Math.PI * (degree / 360)
}

function drawLine(from, to) {
  pen.beginPath();
  pen.moveTo(from.x, from.y);
  pen.lineTo(to.x, to.y);
  pen.stroke();
}

function drawArc(center, radius, startRadians, endRadians, filled) {
  pen.beginPath();
  pen.arc(center.x, center.y, radius, -endRadians, -startRadians);
  if (filled) {
    pen.fill()
  }
  pen.stroke();
}

function drawPolygon(points, stroke, filled) {
  if (points === undefined) {
    console.warn("points undefined in drawPolygon");
    return;
  }

  const amount = points.length;

  pen.beginPath();
  pen.moveTo(points[0].x, points[0].y);
  for (let i = 1; i < amount; i++) {
    pen.lineTo(points[i].x, points[i].y);
  }
  pen.closePath();

  if (stroke) {
    pen.stroke();
  }

  if (filled) {
    pen.fill()
  }
}

function getPolygonLineAtTimeFunction(points, totalTime) {
  const amountLines = (points.length - 1);

  function polygonAtTime(time) {
    if (time === totalTime) {
      return points[points.length - 1];
    }

    const adjustedTime = time * (amountLines / totalTime);
    const moduloTime = realMod(adjustedTime, amountLines);
    const flooredModTime = Math.floor(moduloTime);
    const ceiledModTime = (flooredModTime + 1) % amountLines;
    const modOneTime = moduloTime % 1;

    return {
      x: (1 - modOneTime) * points[flooredModTime].x + modOneTime * points[ceiledModTime].x, // linear interpolation
      y: (1 - modOneTime) * points[flooredModTime].y + modOneTime * points[ceiledModTime].y // linear interpolation
    }
  }

  return polygonAtTime;
}

function getPolygonAtTimeFunction(points, totalTime) {
  return getPolygonLineAtTimeFunction(
    [
      ...points,
      points[0]
    ],
    totalTime
  )
}

function getTimeNow() {
  return new Date().getTime();
}

function rotateXYAround(point, center, radians) {
  const sin = Math.sin(radians);
  const cos = Math.cos(radians);
  return {
    x: (
      center.x
      + (point.x - center.x) * cos
      - (point.x - center.x) * sin
    ),
    y: (
      center.y
      + (point.y - center.y) * sin
      + (point.y - center.y) * cos
    )
  }
}



// TODO

// - shift sphere starting times incrementally (same as the shift of the sphere speeds)

// - endAnimationAfter <- automatically end the animation after this many runs of fullAnimationTimeSeconds <- need to consider how this should behave with startAnimationAtMilli // or maybe after specified seconds


// - Animation Slider:
//   - at bottom, add checkbox to flip between "only considers first circle pulse amount" (which is when all sounds reset if the pulse amounts are integers and there is no weird start time shifting) and "whole animation time" (maybe only possible if it can be calculated with the current shift of speed and starting position), might also show this multiplicative difference in the first view
//   - markers for exciting spots, maybe with slider ridge toggling

// - Preset Menu: Button under the Settings button, opens (slightly) almost full screen dialog, with structure:
//   - at the top lockable settings that will not be changed by picking a preset: instrument, amount moveLines, timing, visuals (individual settings ...), sphere connection lines
//   - each preset with a square (vector graphic) image to click, below some info like instrument, speed, general feel (relaxing, hype, visual(ly experimental), ...); put into some categories: "Inspired by Project JDM", "My own creations", "Created by fans" (<- include name in preset info)
//   - Import / Export???
//   - info on how to submit your own for review and publication

// - Farben: Array an Farben wählbar, dazu bool diskret oder interpolieren, falls interpolieren dann Auswahl, nach wie vielen moveLines wiederholt wird, für beide auch int shift, Button reverse Array an Farben

// - moveLine Farben davon entkoppeln können, damit sie grau sein können mit bunten Kugeln

// - Farben: Silence und Pulse modifiers für Opacity, Lightness, Saturation

// - Farben Kugeln: alle gleiche Farbe, zusätzlich: silenceSameAsMoveLine und pulseSameAsMoveLine

// - Farbrand Kugeln: gleiches wie Farbe innen

// - Instrument Sounds: Auswahl der gespielten Töne mit bool Array (FrontEnd Checkboxen), werden danach geloopt

// - circle scaling width height (not total scaling, only along neutral axis and orthogonal axis)

// DVD mode TODO:
//   - centerBounce <- frame is not adjusted, so the spheres bounce with their center
//   - scaleHeight, scaleWidth <- also scales the speed, so does not affect timing of sounds, whereas the original ratio does affect sound timing
//   - trace: "first" | "all"

// - bool animationRunningIfNegativeTime

// preset ui: bool (default checked?) "Do not reset time on preset change" oder so

