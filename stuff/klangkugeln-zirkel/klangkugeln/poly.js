
// one time only setup stuff
const paper = document.getElementById("paper")
const pen = paper.getContext("2d")



// these things should be recalculated whenever the window size changes
paper.width = paper.clientWidth
paper.height = paper.clientHeight

// create base line
const baseLineLength = Math.min(paper.width, paper.height) * 0.9
const baseLineStart = {
  x: (paper.width - baseLineLength) / 2,
  y: paper.height * 0.5
}
const baseLineEnd = {
  x: baseLineStart.x + baseLineLength,
  y: baseLineStart.y
}
const baseLineMiddle = {
  x: baseLineStart.x + baseLineLength / 2,
  y: baseLineStart.y
}



// audio stuff
let audioContext
let gainNode
function initializeAudioContext() {
  try {
    audioContext = new (window.AudioContext || window.webkitAudioContext)()

    gainNode = audioContext.createGain()
    gainNode.gain.value = 0.2 // audio volume
    gainNode.connect(audioContext.destination)

    try {
      loadAudioBuffers()
    } catch(e) {
      console.warn("Loading audio files failed.")
    }
  } catch(e) {
    console.warn('Web Audio API is not supported in this browser')
  }
}
async function loadAudioBuffers() {
  // build promises to get audioBuffers
  let audioBufferPromises

  audioBufferPromises = paths.map((path, index) => { // just loop over the paths to load the proper amount
    return fetchAudioBufferPromiseFromFile(getAudioPath(index))
  })

  // wait for audio buffer promises and then add audioBuffers to paths
  Promise.all(audioBufferPromises).then(buffers => {
    const newPaths = paths.map((path, index) => ({
      ...path,
      audioBuffer: buffers[index]
    }))

    paths.splice(0, paths.length, ...newPaths)
  })
}
function fetchAudioBufferPromiseFromFile(path) {
  return fetch(path) // return this Promise
    .then((res) => res.arrayBuffer())
    .then((arrayBuffer) => audioContext.decodeAudioData(arrayBuffer))
}
function getAudioPath(index) {
  const modIndex = index % 21
  return `./audio/vibraphone-key-${modIndex}.mp3`
}
function playSound(path) {
  const audioBuffer = path.audioBuffer
  const source = audioContext.createBufferSource() // creates a sound source
  source.buffer = audioBuffer // tell the source which sound to play
  // source.connect(audioContext.destination) // connect the source to the audioContext's destination (the speakers)
  source.connect(gainNode) // now instead of connecting to audioContext.destination, connect to the gainNode, to change volume -> see https://stackoverflow.com/a/43386415
  source.start() // play the source now
}

let soundEnabled = false
document.onvisibilitychange = () => toggleSound(false) // disable sound whenever window gets minimized, else sound will play all at once when you go back to the window, or the frame rendering will be off and the sound plays with weird timing. Not nice either way.
paper.onclick = toggleSound

function toggleSound(bool) {
  // initialize AudioContext, cannot be done before, browsers will block this as part of initially automatic running javascript
  if (audioContext === undefined) {
    initializeAudioContext()
  }
  soundEnabled = ((typeof bool) === "boolean") ? bool : !soundEnabled
  console.log("soundEnabled:", soundEnabled)
}


const startTimeMilli = new Date().getTime();







function draw() {


  pen.strokeStyle = "white"
  pen.lineWidth = 6
  drawStroke(baseLineStart.x, baseLineStart.y, baseLineEnd.x, baseLineEnd.y)


}

draw()








function calculateNextImpactTimeMilli(previousImpactTimeMilli, speed) {
  return previousImpactTimeMilli + (1 / speed) * 1000
}



// drawing helpers
function clearCanvas() {
  pen.clearRect(0, 0, paper.width, paper.height)
}

function drawStroke(startX, startY, endX, endY) {
  pen.beginPath()
  pen.moveTo(startX, startY)
  pen.lineTo(endX, endY)
  pen.stroke()
}

function drawDisk(centerX, centerY, radius) {
  pen.fillStyle = "white"
  pen.beginPath()
  pen.arc(centerX, centerY, radius, 0, 2 * Math.PI)
  pen.fill()
}

function drawArc(centerX, centerY, radius, startAngle, endAngle) {
  pen.beginPath()
  pen.arc(centerX, centerY, radius, startAngle, endAngle)
  pen.stroke()
}

function drawCircle(centerX, centerY, radius) {
  drawArc(centerX, centerY, radius, 0, 2 * Math.PI)
}

