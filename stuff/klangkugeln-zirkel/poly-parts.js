
// one time only setup stuff
const paper = document.getElementById("paper")
const pen = paper.getContext("2d")



// these things should be recalculated whenever the window size changes
paper.width = paper.clientWidth
paper.height = paper.clientHeight

// create base line
const baseLineLength = Math.min(paper.width, paper.height) * 0.9
const baseLineStart = {
  x: (paper.width - baseLineLength) / 2,
  y: paper.height * 0.5
}
const baseLineEnd = {
  x: baseLineStart.x + baseLineLength,
  y: baseLineStart.y
}
const baseLineMiddle = {
  x: baseLineStart.x + baseLineLength / 2,
  y: baseLineStart.y
}



// audio stuff
let audioContext
let gainNode
function initializeAudioContext() {
  try {
    audioContext = new (window.AudioContext || window.webkitAudioContext)()

    gainNode = audioContext.createGain()
    gainNode.gain.value = 0.2; // audio volume
    gainNode.connect(audioContext.destination)

    try {
      loadAudioBuffers()
    } catch(e) {
      console.warn("Loading audio files failed.")
    }
  } catch(e) {
    console.warn('Web Audio API is not supported in this browser')
  }
}
async function loadAudioBuffers() {
  // build promises to get audioBuffers
  let audioBufferPromises

  audioBufferPromises = paths.map((path, index) => { // just loop over the paths to load the proper amount
    return fetchAudioBufferPromiseFromFile(getAudioPath(index))
  })

  // wait for audio buffer promises and then add audioBuffers to paths
  Promise.all(audioBufferPromises).then(buffers => {
    const newPaths = paths.map((path, index) => ({
      ...path,
      audioBuffer: buffers[index]
    }))

    paths.splice(0, paths.length, ...newPaths)
  })
}
function fetchAudioBufferPromiseFromFile(path) {
  return fetch(path) // return this Promise
    .then((res) => res.arrayBuffer())
    .then((arrayBuffer) => audioContext.decodeAudioData(arrayBuffer))
}
function getAudioPath(index) {
  const modIndex = index % 21
  return `./audio/vibraphone-key-${modIndex}.mp3`
}
function playSound(path) {
  const audioBuffer = path.audioBuffer
  const source = audioContext.createBufferSource(); // creates a sound source
  source.buffer = audioBuffer; // tell the source which sound to play
  // source.connect(audioContext.destination); // connect the source to the audioContext's destination (the speakers)
  source.connect(gainNode) // now instead of connecting to audioContext.destination, connect to the gainNode, to change volume -> see https://stackoverflow.com/a/43386415
  source.start(); // play the source now
}

let soundEnabled = false
document.onvisibilitychange = () => toggleSound(false); // disable sound whenever window gets minimized, else sound will play all at once when you go back to the window, or the frame rendering will be off and the sound plays with weird timing. Not nice either way.
paper.onclick = toggleSound

function toggleSound(bool) {
  // initialize AudioContext, cannot be done before, browsers will block this as part of initially automatic running javascript
  if (audioContext === undefined) {
    initializeAudioContext()
  }
  soundEnabled = ((typeof bool) === "boolean") ? bool : !soundEnabled
  console.log("soundEnabled:", soundEnabled)
}




const startTimeMilli = new Date().getTime();



// // #07: Erstelle Pfade mit circleRadius
// const paths = []
// for (let index = 0; index < 21; index++) {
//   const circleRadius = (baseLineLength / 2) * (index / 21)
//
//   paths.push({
//     circleRadius: circleRadius
//   })
// }


// // #09: führe Geschwindigkeit ein (ersetzt 07)
// const paths = []
// for (let index = 0; index < 21; index++) {
//   const circleRadius = (baseLineLength / 2) * ((index + 1) / 21)
//
//   const numberOfLoopsPerFullAnimation = 50 - index
//   const speed = numberOfLoopsPerFullAnimation / 240; // 240 seconds is 4 minutes
//
//   paths.push({
//     circleRadius: circleRadius,
//     speed: speed
//   })
// }


// // #12: führe impact Times ein, für gescheiten Sound (ersetzt 09)
// const paths = []
// for (let index = 0; index < 21; index++) {
//   const circleRadius = (baseLineLength / 2) * ((index + 1) / 21)

//   const numberOfLoopsPerFullAnimation = 50 - index
//   const speed = numberOfLoopsPerFullAnimation / 240; // 240 seconds is 4 minutes
  
//   const nextImpactTime = calculateNextImpactTimeMilli(startTimeMilli, speed)

//   paths.push({
//     circleRadius: circleRadius,
//     speed: speed,
//     lastImpactTime: startTimeMilli,
//     nextImpactTime
//   })
// }





// main loop
function draw() {
  // // #06: Canvas zuerst leer machen
  // clearCanvas()


  // // #04: Zeitmessung
  // const currentTime = new Date().getTime()
  // const elapsedTimeSeconds = (currentTime - startTimeMilli) / 1000
  

  // draw base line  // later move this lower so the base line is on top of the move lines
  pen.strokeStyle = "white"
  pen.lineWidth = 6
  drawStroke(baseLineStart.x, baseLineStart.y, baseLineEnd.x, baseLineEnd.y)


  // // #01: ersten Kreis malen
  // pen.strokeStyle = "gray"
  // pen.lineWidth = 4
  // drawCircle(
  //   baseLineMiddle.x,
  //   baseLineMiddle.y,
  //   50
  // )


  // // #02: erste Kugel malen
  // const centerX = baseLineMiddle.x + 50
  // const centerY = baseLineMiddle.y + 0
  //
  // drawDisk(centerX, centerY, 7)


  // // #05: bewegte Kugel malen (ersetzt 02)
  // const centerX = baseLineMiddle.x + 50 * Math.cos(elapsedTimeSeconds * Math.PI)
  // const centerY = baseLineMiddle.y - 50 * Math.sin(elapsedTimeSeconds * Math.PI)
  //
  // drawDisk(centerX, centerY, 7)


  // // #08: Pfade-Array nutzen (ersetzt 01 und 05)
  // paths.map(currentPath => {
  //   pen.strokeStyle = "gray"
  //   pen.lineWidth = 4
  //   drawCircle(
  //     baseLineMiddle.x,
  //     baseLineMiddle.y,
  //     currentPath.circleRadius
  //   )
  //
  //   const centerX = baseLineMiddle.x + currentPath.circleRadius * Math.cos(elapsedTimeSeconds * Math.PI)
  //   const centerY = baseLineMiddle.y - currentPath.circleRadius * Math.sin(elapsedTimeSeconds * Math.PI)
  //
  //   drawDisk(centerX, centerY, 7)
  // })



    // // #13: Ton abspielen, falls es Zeit dafür ist. Einfügen ganz oben in dem paths.map(currentPath => {...}), damit #14 direkt danach eingefügt werden kann:
    // if (currentTime >= currentPath.nextImpactTime) {
    //   if (soundEnabled) {
    //     playSound(currentPath)
    //   }
    //
    //   // update impact times
    //   currentPath.lastImpactTime = currentPath.nextImpactTime
    //   currentPath.nextImpactTime = calculateNextImpactTimeMilli(currentPath.nextImpactTime, currentPath.speed)
    // }


    // // #14 optional: circle pulse. Das muss vor dem Malen des Kreises stehen. Und die Zeile "pen.strokeStyle = 'gray'" muss entfernt werden:
    // const pulseStrength = 1 - Math.min(
    //   (currentTime - currentPath.lastImpactTime) / 800,
    //   1
    // )
    // const opacity = pulseStrength + (1 - pulseStrength) * 0.35 // linear interpolation between 1 and 0.35
    // const opacity255 = 255 * opacity; // rgb values are between 0 and 255
    // pen.strokeStyle = `rgb(${opacity255}, ${opacity255}, ${opacity255})`


    // // #10: Bestimme zurückgelegte (Bogenlängen-)Distanz und daraus resultierende Kugelmitte (ersetzt die beiden Zeilen mit "centerX = ..." und "centerY = ..." in 08)
    // const distance = (currentPath.speed * elapsedTimeSeconds)
    //
    // const centerX = baseLineMiddle.x + Math.cos(distance * Math.PI) * currentPath.circleRadius
    // const centerY = baseLineMiddle.y - Math.sin(distance * Math.PI) * currentPath.circleRadius

    // // #11: Ton abspielen, naive Version (wird danach wieder gelöscht). Einfügen nach drawDisk(...)
    // // console.log(distance)
    // if (distance % 1 === 0) {
    //   if (soundEnabled) {
    //     playSound(currentPath)
    //   }
    // }



  // // #03: draw() für jeden Frame ausführen
  // // console.log("elapsedTimeSeconds:", elapsedTimeSeconds)
  // requestAnimationFrame(draw)
}

draw()




function calculateNextImpactTimeMilli(previousImpactTimeMilli, speed) {
  return previousImpactTimeMilli + (1 / speed) * 1000
}




// drawing helpers
function clearCanvas() {
  pen.clearRect(0, 0, paper.width, paper.height)
}

function drawStroke(startX, startY, endX, endY) {
  pen.beginPath()
  pen.moveTo(startX, startY)
  pen.lineTo(endX, endY)
  pen.stroke()
}

function drawDisk(centerX, centerY, radius) {
  pen.fillStyle = "white"
  pen.beginPath()
  pen.arc(centerX, centerY, radius, 0, 2 * Math.PI)
  pen.fill()
}

function drawArc(centerX, centerY, radius, startAngle, endAngle) {
  pen.beginPath()
  pen.arc(centerX, centerY, radius, startAngle, endAngle)
  pen.stroke()
}

function drawCircle(centerX, centerY, radius) {
  drawArc(centerX, centerY, radius, 0, 2 * Math.PI)
}



