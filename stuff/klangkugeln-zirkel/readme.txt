Alle Codefetzen liegen in poly-parts.js rum, können in poly.js eingefügt werden. Endergebnis ist in poly-finish.js zu sehen. Basierend auf: https://www.youtube.com/watch?v=Kt3DavtVGVE 'Unravelling the Magic behind Polyrhythms'


TODO verbessern:
- nicht Zweit mit startTimeMilli verarbeiten, sondern mit elapsedTimeSeconds -> vor allem ab Schritt 11 dann auch für calculateNextImpactTime umschreiben von Milli zu Seconds


Plan:

(- zuerst bei allen wordWrap in den Einstellungen anmachen, damit die Codezeilen automatisch umbrechen und nicht rechts rausrutschen)

- anschauen, wie die Koordinaten für die baseLine berechnet werden (ziemlich am Anfang), aber nicht die Kinder selbst machen lassen.

- schnell über Audiokram drüberfliegen, dabei erwähnen, dass das nur so kompliziert ist, damit das Timing schön funktioniert. Einfachere Lösungen haben Timing-Probleme. Außerdem hier toggleSound bei Klick und auch toggleSound(false) für wenn man das Fenster in den Hintergrund rückt, weil die Sounds sonst auch komisch oder später am Stück abgespielt werden

- fast alles wird in der Funktion draw() gemacht. Darin wird bereits die baseLine gemalt. Als erstes wollen wir den Code darin verstehen

- jetzt malen wir einen ersten grauen Kreis. Siehe #01

- dann malen wir eine Kugel, die sich rechts auf dem Kreis und der baseLine befindet. Siehe #02

- jetzt brauchen wir Bewegung: mit requestAnimationFrame(draw) am Ende von draw(). Siehe #03

- für die Animation brauchen wir Zeitmessung. Siehe #04. Betrachte auch die Zeile mit "startTimeMillie = ..."

- jetzt mit sin und cos eine bewegte Kugel malen. Siehe #05

- damit nicht das alte Bild übermalt wird, machen wir vorher den Canvas leer. Siehe #06

- erstelle array aus 21 Pfaden. Anzahl ist 21, weil wir so viele Sound-Dateien haben. Darin speichern wir erstmal nur circleRadius ab. Siehe #07

- setze Pfade ein. Siehe #08. Ersetzt 02 und 05, bzw die werden reingezogen und der Radius, der vorher an 3 Stellen mit 50 hardgecoded war, wird jetzt durch currentPath.circleRadius ersetzt

- passe circleRadius im Pfade-Array leicht an, damit der mittigste Kreis nicht stillsteht. Ändere dafür bei "circleRadius = ...": index -> (index + 1)

- gebe einzelnen Kugeln verschiedene Geschwindigkeiten im Pfade-Array. Verwende dafür die Formel "speed = distance / time", wobei wir bei distance immer die Bogenlänge für einen Halbkreis (also die Bogenlänge zwischen zwei Klängen) als den Wert 1 interpretieren. D.h. wir setzen als Geschwindigkeit einfach, wie viele Halbkreise eine Kugel pro time (in Sekunden) schaffen soll. Siehe #09

- nutze die Geschwindigkeit, um die aktuell zurückgelegte Distanz der einzelnen Kugeln zu bestimmen. Siehe #10

- jetzt wollen wir Ton abspielen. Dafür zuerst naive Variante, die nicht klappt, bzw nur vereinzelte Sounds spielt, weil wir keine stetige Zeitmessung haben, sondern nur ungenau ca so oft pro Minute, wie unser Bildschirm Updates schafft, dazu gedrosselt durch aktuelle Prozessorleistung, denke ich. Siehe #11

- führen impact Times ein im Pfade-Array. Siehe #12

- spielen jetzt Ton ab, wenn es Zeit dafür ist, und updaten die impact Times. Siehe #13

- optional: circle pulse. Siehe #14







